using System;
using System.Diagnostics;
using Newtonsoft.Json.Serialization;

namespace RobinBird.RobinBird.Persistence.Unity
{
	public class JsonUnityTraceWriter : ITraceWriter
	{
		public JsonUnityTraceWriter(TraceLevel level)
		{
			LevelFilter = level;
		}
		
		public void Trace(TraceLevel level, string message, Exception ex)
		{
			string extendedMessage = $"JsonTrace: {message}";
			switch (level)
			{
				case TraceLevel.Error:
					UnityEngine.Debug.LogError(extendedMessage);
					break;
				case TraceLevel.Info:
					UnityEngine.Debug.Log(extendedMessage);
					break;
				case TraceLevel.Off:
					break;
				case TraceLevel.Verbose:
					UnityEngine.Debug.Log(extendedMessage);
					break;
				case TraceLevel.Warning:
					UnityEngine.Debug.LogWarning(extendedMessage);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(level), level, extendedMessage);
			}
		}

		public TraceLevel LevelFilter { get; private set; }
	}
}